public class EveAttribute {
    private String Name;
    private Long Value;
    private String Parent;
    public String getName() {
        return Name;
    }

    public EveAttribute(String name, Long value, String parent) {
        Name = name;
        Value = value;
        Parent = parent;
    }

    public void setName(String name) {
        Name = name;
    }

    public Long getValue() {
        return Value;
    }

    public void setValue(Long value) {
        Value = value;
    }

    public String getParent() {
        return Parent;
    }

    public void setParent(String parent) {
        Parent = parent;
    }
}
