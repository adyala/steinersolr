import java.util.ArrayList;

public class ProductListResponse {

    private ArrayList<Product> products;
    private ArrayList<EveFacet> facets;
    private Long totalCount;

    public ProductListResponse(ArrayList<Product> products, ArrayList<EveFacet> facets, Long totalCount) {
        this.products = products;
        this.facets = facets;
        this.totalCount = totalCount;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public ArrayList<EveFacet> getFacets() {
        return facets;
    }

    public void setFacets(ArrayList<EveFacet> facets) {
        this.facets = facets;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
