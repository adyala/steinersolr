import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class ObjectParser<T> {

    public Product[] productList(String jsonText) {
        Gson gson = new Gson();
        JsonObject parsedJson = gson.fromJson(jsonText , JsonObject.class);
        try {
            JsonObject productsSubset = parsedJson.get("products").getAsJsonObject();
            return gson.fromJson(productsSubset.get("value") , Product[].class);
        } catch (NullPointerException ex) {
            return null;
        }

    }
    public String ParseObject(T var)
    {
        Gson gson = new Gson();
        return gson.toJson(var);
    }

    public int GetRecordCount(String jsonText) {
        Gson gson = new Gson();
        JsonObject parsedJson = gson.fromJson(jsonText, JsonObject.class);
        return parsedJson.get("RecordCount").getAsInt();
    }

}
