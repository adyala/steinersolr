
public class Inventory
{
    
    private String OnHand;
    
    private String StoreId;
    
    private String StoreName;
    
    private String Size;
    
    private String Color;
    
    private String ProductId;
    
    private String Available;
    
    private String WarehouseId;

    public Inventory(String onHand, String storeId, String storeName, String size, String color, String productId, String available, String warehouseId) {
        OnHand = onHand;
        StoreId = storeId;
        StoreName = storeName;
        Size = size;
        Color = color;
        ProductId = productId;
        Available = available;
        WarehouseId = warehouseId;
    }

    public String getOnHand ()
    {
        return OnHand;
    }

    public void setOnHand (String OnHand)
    {
        this.OnHand = OnHand;
    }

    public String getStoreId ()
    {
        return StoreId;
    }

    public void setStoreId (String StoreId)
    {
        this.StoreId = StoreId;
    }

    public String getStoreName ()
    {
        return StoreName;
    }

    public void setStoreName (String StoreName)
    {
        this.StoreName = StoreName;
    }

    public String getSize ()
    {
        return Size;
    }

    public void setSize (String Size)
    {
        this.Size = Size;
    }

    public String getColor ()
    {
        return Color;
    }

    public void setColor (String Color)
    {
        this.Color = Color;
    }

    public String getProductId ()
    {
        return ProductId;
    }

    public void setProductId (String ProductId)
    {
        this.ProductId = ProductId;
    }

    public String getAvailable ()
    {
        return Available;
    }

    public void setAvailable (String Available)
    {
        this.Available = Available;
    }

    public String getWarehouseId ()
    {
        return WarehouseId;
    }

    public void setWarehouseId (String WarehouseId)
    {
        this.WarehouseId = WarehouseId;
    }

    @Override
    public String toString()
    {
        return "[OnHand = "+OnHand+", StoreId = "+StoreId+", StoreName = "+StoreName+", Size = "+Size+", Color = "+Color+", ProductId = "+ProductId+", Available = "+Available+", WarehouseId = "+WarehouseId+"]";
    }
}