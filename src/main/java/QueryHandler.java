import com.google.gson.Gson;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.schema.SchemaRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.schema.SchemaRepresentation;
import org.apache.solr.client.solrj.response.schema.SchemaResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class QueryHandler {

    private HttpSolrClient solr;
    private String solrCore;

    public QueryHandler(HttpSolrClient solr, String solrCore)
    {
        this.solr = solr;
        this.solrCore = solrCore;
    }

    public String querySolr(int skip, ArrayList<String> filters) {
        final SolrQuery query = new SolrQuery("*:*"); //Solr query for all items
        List<Map<String,Object>> attributes = GetAttributeSchema();
        if (attributes != null) {
            for (Map<String, Object> n : attributes) {
                String fieldName = n.get("name").toString();
                if (fieldName.contains("_attr")) {
                    query.addFacetField(fieldName);
                }
            }
        }

        query.setFacetMinCount(1);
        query.addField("ProductId:id");
        query.addField("Name");
        query.addField("SeoName");
        query.addField("variants");
        query.addField("ListPrice");
        query.addField("Price");
        query.addField("DiscountAmount");
        query.addField("Code");
        query.setRows(9);
        query.setStart(skip);

        //Adding filters from request
        for (String filter : filters) { // Adds filters for each key/value passed in
            query.addFilterQuery(filter);
        }
        try {
            final QueryResponse res = solr.query(solrCore , query);
            SolrDocumentList docs = res.getResults();
            ArrayList<Product> products = MapResponseToObject(docs);
            ArrayList<EveFacet> facets = GenerateFacetList(res.getFacetFields());
            Long totalCount = docs.getNumFound();
            ProductListResponse productListResponse = new ProductListResponse(
                    products, facets, totalCount
            );
            Gson gson = new Gson();
            return gson.toJson(productListResponse);

        } catch (Exception ex) {
            return ex.toString();
        }
    }

    /**
     * Creates a schema request from the SOLR server to get entire schema
     * @return Schema representation for the Solr core
     */
    private List<Map<String,Object>> GetAttributeSchema() {
        try {
            SchemaRequest schemaRequest = new SchemaRequest();
            SchemaResponse schemaResponse = schemaRequest.process(solr , solrCore);
            SchemaRepresentation schema = schemaResponse.getSchemaRepresentation();
            return schema.getFields();
        } catch (Exception ex) {
            System.out.println(ex);
        }
       return null;
    }

    public String ParseQuery(String query) {
        if (query.contains("brand-")) {
            query = query.replace("brand-" , "");
            query = query.toLowerCase();
        }
        ArrayList<String> parsedQuery = new ArrayList<>(); //Parsed portion of the query
        ArrayList<String> unparsedQuery = new ArrayList<>(); //Unparsed portion of the query
        final SolrQuery solrQuery = new SolrQuery("*:*"); //Solr query for all items
        List<Map<String,Object>> attributes = GetAttributeSchema();
        if (attributes != null) {
            for (Map<String, Object> n : attributes) {
                String fieldName = n.get("name").toString();
                if (fieldName.contains("_attr")) {
                    solrQuery.addFacetField(fieldName); //Adds attribute query portions to the string
                }
            }
        }
        try {

            final QueryResponse res = solr.query(solrCore, solrQuery); //Performs full solr query
            List<FacetField> facetFields = res.getFacetFields();
            List<String> facetValues = new ArrayList<>();
            for (FacetField f : facetFields) {
                List<FacetField.Count> vc = f.getValues();
                for (FacetField.Count c : vc) {
                    String cReplaced = c.getName().toLowerCase();
                    facetValues.add(f.getName() + ":" + cReplaced); //Puts all facets into a list with facetName:facetValue
                }
            }
            while (!query.isEmpty()) {


                String foundQueryParam = "";
                for (String fv : facetValues) { //Searches the facets for the facet value
                    if (fv.contains(query.replaceAll("_" , "-"))) {
                        foundQueryParam = fv;
                        break;
                    }
                }

            if (!foundQueryParam.isEmpty()) { //If we found the facet param with the query string we add it to the parsed query
                parsedQuery.add(foundQueryParam);
                if (!unparsedQuery.isEmpty()) {

                    StringBuilder queryRebuilder = new StringBuilder();
                    for (int i = unparsedQuery.size() - 1; i >= 0; i--) {
                        queryRebuilder.append(unparsedQuery.get(i));
                        if (i > 0) {
                            queryRebuilder.append("-");
                        }

                    }
                    query = queryRebuilder.toString();
                    unparsedQuery.clear();
                } else {
                    break;
                }
            } else {
                String[] splitQuery = query.split("-");
                splitQuery[splitQuery.length - 1] = splitQuery[splitQuery.length - 1];
                unparsedQuery.add(splitQuery[splitQuery.length - 1]);
                splitQuery[splitQuery.length - 1] = "";
                query = String.join("-", splitQuery);
                if (query.length() > 0) {
                    query = query.substring(0 , query.length() -1);
                }

            }
        }
        } catch(Exception ex) {
            return ex.toString();
        }

        return querySolr(0 , parsedQuery);

        }




    /**
     * Generates facet list from solr response
     * @param facets facets returned from solr response
     * @return Array list of facets and counts
     */
    private ArrayList<EveFacet> GenerateFacetList(List<FacetField> facets) {
        ArrayList<EveFacet> eveFacets = new ArrayList<>();
        for (FacetField f : facets) {
            EveFacet fac = new EveFacet();
            ArrayList<EveAttribute> attributes = new ArrayList<>();
            String facetName = f.getName();
            fac.setFacetName(facetName.substring(0 , facetName.length() - 5).replace("_" , " "));
            List<FacetField.Count> tempCounts = f.getValues();
            for (FacetField.Count c : tempCounts) {
                attributes.add(new EveAttribute(c.getName(), c.getCount(), f.getName()));
            }

            fac.setFacetValue(attributes);
            eveFacets.add(fac);
        }
        return eveFacets;
    }

    /**
     * Maps solr response to document list
     * @param documents Documents found from solr query
     * @return Array list of products mapped from documents
     */
    private ArrayList<Product> MapResponseToObject(SolrDocumentList documents) {
        ArrayList<Product> products = new ArrayList<>();
        Gson gson = new Gson();
        for (SolrDocument doc : documents) {

            Product product = new Product();
            product.setProductId(Long.parseLong(doc.get("ProductId").toString()));
            product.setName(doc.get("Name").toString().replace("[" , "").replaceAll("]" , ""));
//            product.setCode(doc.get("Code").toString().replace("[" , "").replaceAll("]" , ""));
            product.setPrice(Double.parseDouble(doc.get("Price").toString().replace("[" , "").replaceAll("]" , "")));
            product.setListPrice(Double.parseDouble(doc.get("ListPrice").toString().replace("[" , "").replaceAll("]" , "")));
//            product.setSEOUrl((doc.get("SEOUrl").toString().replace("[" , "").replaceAll("]" , "")));
//            product.setDiscountAmount(doc.get("DiscountAmount").toString());

//            Variants[] variants = gson.fromJson(doc.get("variants").toString(), Variants[].class);
//            product.setVariants(variants);
            products.add(product);
        }
        return products;
    }
}
