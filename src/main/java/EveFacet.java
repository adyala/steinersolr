import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EveFacet {

    private String FacetName;
    private ArrayList<EveAttribute> FacetValue;

    public String getFacetName() {
        return FacetName;
    }

    public void setFacetName(String facetName) {
        FacetName = facetName;
    }

    public ArrayList<EveAttribute> getFacetValue() {
        return FacetValue;
    }

    public void setFacetValue(ArrayList<EveAttribute> facetValue) {
        FacetValue = facetValue;
    }
}

