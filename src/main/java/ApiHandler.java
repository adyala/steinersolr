

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class ApiHandler {

    /**
     * Gets products from database
     * @param top Number of products to get
     * @param skip Number of products to skip
     * @return Products as string
     */
    public String getProducts(int top, int skip) {


        try {
                Map<String, String> params = new HashMap<>();
                params.put("$top" , Integer.toString(top));
                params.put("$skip" , Integer.toString(skip));

                URL url = new URL("http://steiner-dev.local/api/products/products_list?" + BuildParameters((params)));
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
//            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; U; Android 7.1");

                con.setRequestProperty("Content-Type" , "application/json");
                con.setRequestProperty("Access-Control-Allow-Origin" , "*");
                con.setConnectTimeout(5000);
                con.setReadTimeout(10000);

//                con.setDoOutput(true);
//                DataOutputStream out = new DataOutputStream(con.getOutputStream());
//
//                out.flush();
//                out.close();

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();


            return content.toString();
        } catch(Exception ex) {
            return null;
        }

    }


    /**
     * Builds url encoded parameters
     * @param params Parameters to encode
     * @return Parameter String
     */
    private String BuildParameters(Map<String, String> params) {

        try {
            StringBuilder result = new StringBuilder();

            for (Map.Entry<String, String> entry : params.entrySet()) {
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                result.append("&");
            }

            String resultString = result.toString();
            return resultString.length() > 0
                    ? resultString.substring(0, resultString.length() - 1)
                    : resultString;
        } catch (UnsupportedEncodingException ex) {
            System.out.println(ex);
        }
        return null;
    }
}
