import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.NoOpResponseParser;
import org.apache.solr.client.solrj.request.LukeRequest;
import org.apache.solr.client.solrj.request.schema.SchemaRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.LukeResponse;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.schema.SchemaRepresentation;
import org.apache.solr.client.solrj.response.schema.SchemaResponse;
import org.apache.solr.common.SolrDocumentList;

import java.sql.*;
import java.util.*;

public class HelloWorld {
    public static void main(String[] args) {
        Conn();
        HttpSolrClient solr = getSolrClient();

        Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).start(7000);


        app.get("/", ctx -> {
            QueryHandler handler = new QueryHandler(solr, "steinerproducts");
            int skip = 0;
            ArrayList<String> filters = new ArrayList<>();
            String skipParam = ctx.queryParam("skip");
            String filtersParam = ctx.queryParam("filters");
            try {
                if (skipParam != null) {
                    skip = Integer.parseInt(skipParam);
                }
                if (filtersParam != null) {
                    String[] splitFilters = filtersParam.split(",");
                    filters.addAll(Arrays.asList(splitFilters));
                }

            } catch (Exception ex) {
                ctx.status(400);
                ctx.result("Invalid Request");
            }


            ctx.result(handler.querySolr(skip, filters));

        });
        app.get("/index", ctx -> {
            ArrayList<String> indexFields = new ArrayList<>();
            indexFields.add("Brands List");
            indexFields.add("SubBrands List");
            indexFields.add("Models List");
            indexFields.add("Part Type");
            indexFields.add("Sub Part Type");
            IndexHandler handler = new IndexHandler(solr, "steinerproducts", indexFields);
            int productsIndexed = handler.IndexProducts();

            ctx.result("Products Indexed " + productsIndexed);
        });

        app.get("/parse", ctx -> {
            QueryHandler handler = new QueryHandler(solr, "steinerproducts");
            ctx.result(handler.ParseQuery("brand-allis-chalmers-ca"));
        });
    }


    private static HttpSolrClient getSolrClient() {
        return new HttpSolrClient.Builder("http://localhost:8983/solr/")
                .withConnectionTimeout(10000)
                .withSocketTimeout(60000)
                .build();
    }
    public static void Conn() {


        Connection conn;
        try {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        conn = DriverManager.getConnection("jdbc:sqlserver://192.168.128.55:14333;databaseName=e4commerce", "sa", "@even!caMSSQL2018");

        if(conn!=null) {
            System.out.println("Database Successfully connected");
            String query = "use e4commerce; SELECT * FROM commerce.Product";

            Statement st  = conn.createStatement();
            ResultSet resultSet = st.executeQuery(query);
            while (resultSet.next()) {
                System.out.println(resultSet.getString("Name"));
            }
        }



        } catch (Exception e) {
        e.printStackTrace();
        }


    }
}