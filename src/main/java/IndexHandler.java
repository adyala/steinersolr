import com.google.gson.Gson;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrInputDocument;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class IndexHandler {

    private HttpSolrClient solr;
    private String solrCore;
    private ArrayList<String> indexFields;

    public IndexHandler(HttpSolrClient solr, String solrCore, ArrayList<String> indexFields) {
        this.solr = solr;
        this.solrCore = solrCore;
        this.indexFields = indexFields;
    }

    public int IndexProducts() {
        ApiHandler handler = new ApiHandler();
        ObjectParser parser = new ObjectParser();
        int top = 30;
        int skip = 0;
        String jsonString = handler.getProducts(top, skip);
        int recordCount = parser.GetRecordCount(jsonString);
        Product[] productSet = parser.productList(jsonString);
        ArrayList<Product> products = new ArrayList<>(Arrays.asList(productSet));
        if (recordCount > 0) {
            int page = 1;
            int retries = 3;
            int currentTry = 1;
            while (skip<= recordCount) {
                int prevSkip = skip;
                skip = page * top;
                page++;
                jsonString = handler.getProducts(top, skip);
                Product[] nextProductSet = parser.productList(jsonString);
                if (nextProductSet == null) {
                    if (currentTry > retries) {
                        break;
                    }
                    currentTry++;
                    skip = prevSkip;
                    continue;
                }
                products.addAll(Arrays.asList(nextProductSet));
                System.out.println(products.size());
            }

            indexProducts(products);
        }
        return products.size();
    }

    private void indexProducts(ArrayList<Product> products) {
        try {
            for (Product p : products) {
                SolrInputDocument doc = _mapObjectToDocument(p);
                solr.add(solrCore , doc);
            }
            solr.commit(solrCore);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private SolrInputDocument _mapObjectToDocument(Product product) {
        Gson gson = new Gson();
        ObjectParser<Attribute> parser = new ObjectParser<>();
        final SolrInputDocument doc = new SolrInputDocument();
        doc.addField("id" , product.getProductId());
        doc.addField("Name" , product.getName());
//        doc.addField("CdnURL" , product.getCdnURL());
        doc.addField("ListPrice" , product.getListPrice());
        doc.addField("Price" , product.getPrice());
//        doc.addField("Code" , product.getCode());
        doc.addField("SEOUrl" , product.getSEOUrl());
        doc.addField("type_s" , "parent");
        ArrayList<String> alreadyAdded = new ArrayList<>();
        for (Attribute a : product.getAttributes())
        {
            doc.addField("Attributes" , parser.ParseObject(a));

            String val = a.getValue();
            if (!alreadyAdded.contains(val) && indexFields.contains(a.getName())) {
                alreadyAdded.add(val);
                doc.addField(a.getName() + "_attr" , val.replaceAll(" " , "_ "));
            }
//
        }

        return doc;
    }
}
