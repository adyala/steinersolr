public class Product
{
    private Attribute[] Attributes;
    private String[] brands;
    private Double DiscountAmount;
    private String image;
    private Inventory inventory;
    private Double ListPrice;
    private String Name;
    private Double Price;
    private Long ProductId;
    private String ProductStatusName;
    private String ProductTypeName;
    private Double Ratings;
    private String SEOUrl;
    private String UnitOfMeasure;
    private UnitOfMeasure[] UnitOfMeasures;
    private String UrlName;

    public Attribute[] getAttributes() {
        return Attributes;
    }

    public void setAttributes(Attribute[] attributes) {
        this.Attributes = attributes;
    }

    public String[] getBrands() {
        return brands;
    }

    public void setBrands(String[] brands) {
        this.brands = brands;
    }

    public Double getDiscountAmount() {
        return DiscountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        DiscountAmount = discountAmount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Double getListPrice() {
        return ListPrice;
    }

    public void setListPrice(Double listPrice) {
        ListPrice = listPrice;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double price) {
        Price = price;
    }

    public Long getProductId() {
        return ProductId;
    }

    public void setProductId(Long productId) {
        ProductId = productId;
    }

    public String getProductStatusName() {
        return ProductStatusName;
    }

    public void setProductStatusName(String productStatusName) {
        ProductStatusName = productStatusName;
    }

    public String getProductTypeName() {
        return ProductTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        ProductTypeName = productTypeName;
    }

    public Double getRatings() {
        return Ratings;
    }

    public void setRatings(Double ratings) {
        Ratings = ratings;
    }

    public String getSEOUrl() {
        return SEOUrl;
    }

    public void setSEOUrl(String SEOUrl) {
        this.SEOUrl = SEOUrl;
    }

    public String getUnitOfMeasure() {
        return UnitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        UnitOfMeasure = unitOfMeasure;
    }

    public UnitOfMeasure[] getUnitOfMeasures() {
        return UnitOfMeasures;
    }

    public void setUnitOfMeasures(UnitOfMeasure[] unitOfMeasures) {
        UnitOfMeasures = unitOfMeasures;
    }

    public String getUrlName() {
        return UrlName;
    }

    public void setUrlName(String urlName) {
        UrlName = urlName;
    }
}
